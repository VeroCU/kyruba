<?php
  $campo1="tyct".$id;
  $campo2="tyc".$id;

  $CONSULTA    = $CONEXION -> query("SELECT * FROM configuracion WHERE id = 1");
  $rowCONSULTA = $CONSULTA -> fetch_assoc();
  $title=$rowCONSULTA[$campo1];
  $txt=$rowCONSULTA[$campo2];
?>
<!DOCTYPE html>
<html lang="<?=$languaje?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>">
  
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="<?=$description?>">
  <meta property="og:url" content="<?=$rutaEstaPagina?>">
  <meta property="og:image" content="<?=$ruta?>img/design/logo-og.jpg">
  <meta property="fb:app_id" content="<?=$appID?>">

  <?=$headGNRL?>

</head>

<body>

<?=$header?>

<div class="uk-container uk-container-small padding-v-100">
  <div class="uk-card uk-card-default uk-card-body uk-border-rounded uk-text-justify min-height-500px">
    <h3><?=$title?></h3>
    <?=$txt?>
  </div>
</div>

<?=$footer?>

<?=$scriptGNRL?>

</body>
</html>