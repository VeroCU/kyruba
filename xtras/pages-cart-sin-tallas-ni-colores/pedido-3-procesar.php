<?php
if(isset($_SESSION['carro']) AND count($arreglo)>0){

  // Requiere factura
  $requiereFactura=(isset($_SESSION['requierefactura']))?$_SESSION['requierefactura']:0;

  // Envío Global
  $shippingGlobalPayPal = $shippingGlobal;

  // Formar la tabla del pedido
  $style[0]='style="background-color:#EEEEEE;"';
  $style[1]='style="background-color:#FFF;"';
  $num=0;
  $tabla='
      <table style="width: 840px;" cellspacing="2" border="0" bgcolor="#CCC">
        <tr '.$style[1].'>
          <th style="padding: 8px; color: #666; width: 300px; text-align: left;  ">Producto</th>
          <th style="padding: 8px; color: #666; width: 180px; text-align: center;">Cantidad</th>
          <th style="padding: 8px; color: #666; width: 180px; text-align: right; ">Precio</th>
          <th style="padding: 8px; color: #666; width: 180px; text-align: right; ">Importe</th>
        </tr>'; 

  $subtotal=0;
  $count=0;


  $sql = "INSERT INTO pedidos (uid,fecha,factura) VALUES ('$uid','$ahora','$requiereFactura')";
  if($insertar = $CONEXION->query($sql)){
    $pedidoId=$CONEXION->insert_id;
    $idmd5=md5($pedidoId);

    // Build the query string
    $queryString  = "?cmd=_cart";
    $queryString .= "&upload=1";
    $queryString .= "&trackingId=".$pedidoId;
    $queryString .= "&noshipping=0";
    $queryString .= "&charset=utf-8";
    $queryString .= "&currency_code=MXN";
    $queryString .= "&business=" . urlencode($payPalCliente);
    $queryString .= "&return=".$ruta . 'success';
    $queryString .= "&notify_url=".$ruta . urlencode($idmd5.'_IPN');
    $queryString .= "&cancel_return=".$ruta . urlencode('mi-cuenta');


    foreach ($arreglo as $key) {
      $prodId=$key['Id'];
      
      $CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $prodId");
      $row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
      
      $importe=$row_CONSULTA1['precio']*$key['Cantidad'];
      $subtotal+=$importe;



      $cantidad=$key['Cantidad'];
      $productotxt=$row_CONSULTA1['sku'].' - '.$row_CONSULTA1['titulo'];
      $precio=$row_CONSULTA1['precio'];

      $sql = "INSERT INTO pedidosdetalle (pedido,producto,productotxt,cantidad,precio,importe)".
        "VALUES ('$pedidoId','$prodId','$productotxt','$cantidad','$precio','$importe')";
      $insertar = $CONEXION->query($sql);

      // Descuenta del inventario
      $existenciasRestantes=$row_CONSULTA1['existencias']-$cantidad;
      $actualizar = $CONEXION->query("UPDATE productos SET existencias = '$existenciasRestantes' WHERE id = $prodId");

      $count++;

      $queryString .= '&item_number_' . $count . '=' . urlencode($prodId);
      $queryString .= '&item_name_' . $count . '=' . urlencode($productotxt);
      $queryString .= '&amount_' . $count . '=' . urlencode(($precio)*(1+$taxIVA));
      $queryString .= '&quantity_' . $count . '=' . urlencode($cantidad);
      $queryString .= '&shipping_' . $count . '=' . urlencode((($shipping*$cantidad)+$shippingGlobalPayPal)*(1+$taxIVA));

      $shippingGlobalPayPal = 0;


      $num++; 
      if ($num==2) { $num=0; }
      $tabla.='
        <tr '.$style[$num].'>
          <td style="padding: 8px; text-align: left; ">
            '.$row_CONSULTA1['sku'].' -
            '.$row_CONSULTA1['titulo'].'
          </td>
          <td style="padding: 8px; text-align: center; ">
            '.$key['Cantidad'].'
          </td>
          <td style="padding: 8px; text-align: right; ">
            '.number_format($row_CONSULTA1['precio'],2).'
          </td>
          <td style="padding: 8px; text-align: right; ">
            '.number_format($importe,2).'
          </td>
        </tr>'; 
    }
  }

  $envio=$shipping*$carroTotalProds;
  $subtotal=$subtotal+$envio+$shippingGlobal;
  $iva=($taxIVA>0)?$subtotal*$taxIVA:0;
  $total=$subtotal+$iva;

  if ($total>0) {
    if ($shippingGlobal>0) {
      $num=($num==0)?1:0;
      $tabla.='
      <tr '.$style[$num].'>
        <td style="padding: 8px; text-align: left; ">
          Envío global
        </td>
        <td style="padding: 8px; text-align: center; ">
          1
        </td>
        <td style="padding: 8px; text-align: right; ">
          '.number_format($shippingGlobal,2).'
        </td>
        <td style="padding: 8px; text-align: right; ">
          '.number_format($shippingGlobal,2).'
        </td>
      </tr>';
    }
    if ($shipping>0) {
      $num=($num==0)?1:0;
      $tabla.='
      <tr '.$style[$num].'>
        <td style="padding: 8px; text-align: left;">
          Envío por pieza
        </td>
        <td style="padding: 8px; text-align: center;">
          '.$carroTotalProds.'
        </td>
        <td style="padding: 8px; text-align: right;">
          '.number_format($shipping,2).'
        </td>
        <td style="padding: 8px; text-align: right;">
          '.number_format($envio,2).'
        </td>
      </tr>';
    }

    if ($taxIVA>0) {
      $num=($num==0)?1:0;
      $tabla.='
      <tr>
        <td colspan="3" style="padding: 8px;text-align:right;">
          Subtotal
        </td>
        <td style="padding: 8px;text-align:right;">
          '.number_format($subtotal,2).'
        </td>
      </tr>
      <tr>
        <td colspan="3" style="padding: 8px;text-align:right;">
          IVA
        </td>
        <td style="padding: 8px;text-align:right;">
          '.number_format($iva,2).'
        </td>
      </tr>';
    }

    $num=($num==0)?1:0;
    $tabla.='
      <tr '.$style[$num].'>
        <td colspan="3" style="padding: 8px;text-align:right;">
          Total
        </td>
        <td style="padding: 8px;text-align:right;">
          '.number_format($total,2).'
        </td>
      </tr>
      ';
  }

  $tabla.='
    </table>';

	$actualizar = $CONEXION->query("UPDATE pedidos SET 
    idmd5 = '$idmd5',
    tabla = '$tabla',
    importe = '$total',
    cantidad = '$carroTotalProds'
    WHERE id = $pedidoId");

	unset($_SESSION['carro']);


  $CONSULTA = $CONEXION -> query("SELECT * FROM usuarios WHERE id = '$uid'");
  $numUser=$CONSULTA->num_rows;
  if ($numUser>0) {
    $row_CONSULTA = $CONSULTA -> fetch_assoc();

    // Almacenar domicilio de entrega
      $domNum=(isset($_SESSION['domicilio2']) AND $_SESSION['domicilio2']==1)?2:'';

      $nombre = $row_CONSULTA['nombre'];
      $email = $row_CONSULTA['email'];
      $calle = $row_CONSULTA['calle'.$domNum];
      $noexterior = $row_CONSULTA['noexterior'.$domNum];
      $nointerior = $row_CONSULTA['nointerior'.$domNum];
      $entrecalles = $row_CONSULTA['entrecalles'.$domNum];
      $pais = $row_CONSULTA['pais'.$domNum];
      $estado = $row_CONSULTA['estado'.$domNum];
      $municipio = $row_CONSULTA['municipio'.$domNum];
      $colonia = $row_CONSULTA['colonia'.$domNum];
      $cp = $row_CONSULTA['cp'.$domNum];

      $actualizar = $CONEXION->query("UPDATE pedidos SET
        nombre = '$nombre',
        email = '$email',
        calle = '$calle',
        noexterior = '$noexterior',
        nointerior = '$nointerior',
        entrecalles = '$entrecalles',
        pais = '$pais',
        estado = '$estado',
        municipio = '$municipio',
        colonia = '$colonia',
        cp = '$cp'
        WHERE id = $pedidoId");

      unset($_SESSION['domicilio2']);

    $nombre=$row_CONSULTA['nombre'];
    $email =$row_CONSULTA['email'];
    $send2user=1;
    $colorPrimary='#333';
    $asunto='Orden #'.$pedidoId.' en '.$Brand;
    $cuerpoMensaje='
      <div style="width:700px;">
        <div style="width:100%;padding-top:20px;color:'.$colorPrimary.';font-size:22px;">
          <b>Gracias por su compra</b>
        </div>
        <div style="width:100%;padding-top:20px;color:'.$colorPrimary.';font-size:17px;">
          A continuaci&oacute;n el resumen de su compra:
        </div>
        <div style="width:100%;padding-top:30px;">
          <a href="'.$ruta.$idmd5.'_revisar.pdf" style="background-color:'.$mailButton.';color:white;text-decoration:none;border-radius:8px;padding:13px;font-weight:700;">Versi&oacute;n PDF</a>
        </div>
        <div style="width:100%;padding-top:30px;">
          '.$tabla.'
        </div>
        <div style="width:100%;text-align:center;padding-top:50px;padding-bottom:50px;font-size:16px;">
          <a style="color:'.$colorPrimary.';text-decoration:none;" href="mailto:'.$destinatario1.'">'.$destinatario1.'</a><br><br>
          <a style="color:'.$colorPrimary.';text-decoration:none;" href="'.$ruta.'">www.'.$dominio.'</a>
        </div>
      </div>';
    //include 'includes/sendmail.php';
  }



  if (isset($_GET['deposito'])) {
      header('Location: ' .$idmd5.'_detalle');
  }else{
    //echo 'https://www.paypal.com/cgi-bin/webscr'.$queryString;

    if ($sandbox==0) {
      header('Location: https://www.paypal.com/cgi-bin/webscr' .$queryString);
    }else{
      header('Location: https://www.sandbox.paypal.com/cgi-bin/webscr' .$queryString);
    }

  }
}
