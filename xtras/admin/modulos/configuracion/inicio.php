<?php
$subsecciones[] = array(
	  'title' => 'Tallas',
 'subseccion' => 'cfgtallaclasif',
	   'icon' => 'file');

$subsecciones[] = array(
	  'title' => 'Colores',
 'subseccion' => 'cfgcolores',
	   'icon' => 'palette');

$subsecciones[] = array(
	  'title' => 'Marcas',
 'subseccion' => 'cfgmarcas',
	   'icon' => 'copyright');

$subsecciones[] = array(
	  'title' => 'Catálogo',
 'subseccion' => 'catalogo',
	   'icon' => 'file-pdf');

$subsecciones[] = array(
	  'title' => 'Contacto',
 'subseccion' => 'contacto',
	   'icon' => 'envelope');

$subsecciones[] = array(
	  'title' => 'FAQ',
 'subseccion' => 'faq',
	   'icon' => 'question');

$subsecciones[] = array(
	  'title' => 'Generales',
 'subseccion' => 'general',
	   'icon' => 'cogs');

$subsecciones[] = array(
	  'title' => 'Nosotros',
 'subseccion' => 'about',
	   'icon' => 'copyright');

$subsecciones[] = array(
	  'title' => 'Políticas',
 'subseccion' => 'politicas',
	   'icon' => 'info');

$subsecciones[] = array(
	  'title' => 'Slider',
 'subseccion' => 'slider',
	   'icon' => 'images');

$subsecciones[] = array(
	  'title' => 'Usuarios',
 'subseccion' => 'usuarios',
	   'icon' => 'users');


echo '
<div class="uk-width-auto@m margin-top-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'" class="color-red">Configuración</a></li>
	</ul>
</div>




<div class="uk-width-1-1">
	<div class="uk-container">
		<div uk-grid class="uk-flex-center" style="margin-top: 30px;">';

		foreach ($subsecciones as $key => $value) {
			echo '
			<div class="uk-width-auto">
				<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$value['subseccion'].'">
					<div class="uk-card uk-card-default uk-flex uk-flex-center uk-flex-middle uk-text-center uk-text-capitalize" style="width: 200px;height: 200px;">
						<div>
							<i class="fa fa-3x fa-'.$value['icon'].'"></i>
							<br><br>
							'.$value['title'].'
						</div>
					</div>
				</a>
			</div>';
		}

	echo '
		</div>
	</div>
</div>';



