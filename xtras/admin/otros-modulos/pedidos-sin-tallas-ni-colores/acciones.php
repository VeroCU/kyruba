<?php 
	date_default_timezone_set('America/Mexico_City');
	$fecha=date('Y-m-d');


//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar pedido 
	if(isset($_REQUEST['borrarPedido'])){

		$CONSULTA = $CONEXION -> query("SELECT * FROM pedidosdetalle WHERE pedido = $id");
		while($row_CONSULTA = $CONSULTA -> fetch_assoc()){
			$prodId=$row_CONSULTA['producto'];
			$existenciasAgregar=$row_CONSULTA['cantidad'];
			$CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $prodId");
			while($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()){
				$existenciasNuevas=$row_CONSULTA1['existencias']+$existenciasAgregar;
				if ($_GET['incorporar']==1) {
					$actualizar = $CONEXION->query("UPDATE productos SET existencias = $existenciasNuevas WHERE id = $prodId");
				}
			}
		}

		if($borrar = $CONEXION->query("DELETE FROM pedidos WHERE id = $id"))
		{
			$borrar = $CONEXION->query("DELETE FROM pedidosdetalle WHERE pedido = $id");
			$borrar = $CONEXION->query("DELETE FROM ipn WHERE pedido = $id");
			$exito='success';
			$legendSuccess.="<br>Pedido eliminado";
		}else{
			$fallo='danger';  
			$legendFail.="<br>No se pudo eliminar";
		}
	} 

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Estatus 
	if (isset($_POST['estatuschange'])) {
		include '../../../includes/connection.php';

		$id = $_POST['id'];
		$estatus = $_POST['estatus'];

		if($actualizar = $CONEXION->query("UPDATE pedidos SET estatus = $estatus WHERE id = $id")){
			$mensajeClase='success';
			$mensajeIcon='check';
			$mensaje='Guardado';
		}else{
			$mensajeClase='danger';
			$mensajeIcon='ban';
			$mensaje='No se pudo guardar';
		}
		echo '<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>';		
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Envío de correos
	if (isset($_POST['enviarcorreo'])) {
		$enviarcorreo=$_POST['enviarcorreo'];
		$thisid=$_POST['id'];

		$rutaConnection	= "../../../includes/connection.php";
		require $rutaConnection;
		$ruta=($dominio=='wozialmarketinglovers.com')?$protocolo.$dominio.'/ecy/':$protocolo.$dominio.'/';

		$CONSULTA = $CONEXION -> query("SELECT * FROM pedidos WHERE id = $thisid");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		$user=$row_CONSULTA['uid'];

		$CONSULTA1 = $CONEXION -> query("SELECT * FROM usuarios WHERE id = $user");
		$row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();

		switch ($enviarcorreo) {
			case 1:
				$asunto = 'Orden No. '.$thisid.' enviada'; 
				$cuerpoMensaje = '
					Estimado <b>'.$row_CONSULTA1['nombre'].'</b><br><br>
					Los productos solicitados en la orden <b>#'.$thisid.'</b> han sido enviados a su domicilio.<br><br>
					Su n&uacute;mero de gu&iacute;a es el <b>'.$row_CONSULTA['guia'].'</b><br><br>
					Si desea ver su pedido puede hacerlo en el suiguiente enlace:<br><br><br><br><br>
					<a href="'.$ruta.'Mi-Cuenta" style="background-color:'.$mailButton.';font-weight:700;border-radius:8px;padding-left:30px;padding-right:30px;padding-top:10px;padding-bottom:10px;color:white;text-decoration:none;">Mi cuenta</a><br><br><br><br>
					o copie y pegue este enlace: <br>
					<a href="'.$ruta.'Mi-Cuenta">'.$ruta.'Mi-Cuenta</a><br><br>
					Saludos cordiales.';
				break;
			case 2:
				$asunto = 'Orden No. '.$thisid; 
				$cuerpoMensaje = '
					Estimado <b>'.$row_CONSULTA1['nombre'].'</b><br><br>
					Tenemos registrada una orden de compra con el n&uacute;mero <b>'.$thisid.'</b> por un importe de $'.number_format($row_CONSULTA['importe'],2).'<br><br>
					Si ya realiz&oacute; el pago, favor de subir su comprobante en su panel de cliente en el siguiente enlace:<br><br><br>
					<a href="'.$ruta.'Mi-Cuenta" style="background-color:'.$mailButton.';font-weight:700;border-radius:8px;padding-left:30px;padding-right:30px;padding-top:10px;padding-bottom:10px;color:white;text-decoration:none;">Mi cuenta</a><br><br><br><br>
					o copie y pegue este enlace: <br>
					<a href="'.$ruta.'Mi-Cuenta">'.$ruta.'Mi-Cuenta</a><br><br><br>
					También puede enviarlo al siguiente correo:<br>
					'.$destinatario1.'<br><br>
					Saludos cordiales.';
				break;
			case 3:
				$asunto = 'Orden No. '.$thisid.' cancelada'; 
				$cuerpoMensaje = '
					Estimado <b>'.$row_CONSULTA1['nombre'].'</b><br><br>
					La orden <b>'.$thisid.'</b> ha sido cancelada.<br><br>
					En caso de desear adquirir los productos solicitados deber&aacute; levantar una nueva orden.<br><br>
					Si desea ver sus pedidos puede hacerlo en el suiguiente enlace:<br><br><br><br>
					<a href="'.$ruta.'Mi-Cuenta" style="background-color:'.$mailButton.';font-weight:700;border-radius:8px;padding-left:30px;padding-right:30px;padding-top:10px;padding-bottom:10px;color:white;text-decoration:none;">Mi cuenta</a><br><br><br><br>
					o copie y pegue este enlace: <br>
					<a href="'.$ruta.'Mi-Cuenta">'.$ruta.'Mi-Cuenta</a><br><br>
					Saludos cordiales.';
				break;
		}
		$logo=$ruta.'img/design/logo-mail.png';

		$sendMail=1;
		include 'sendmail.php';
		echo $mensaje;
	}


if (file_exists('error_log')) {
	unlink('error_log');
}
