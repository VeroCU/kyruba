<?php
echo '
<div class="uk-width-1-2@m margen-v-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=importar" class="color-red">Importar</a></li>
	</ul>
</div>
<div class="uk-width-1-2@m margen-v-20 uk-text-right">
	<button class="uk-button uk-button-danger" id="eliminatodo"><i uk-icon="trash"></i> &nbsp; Borrar todo</button>
</div>
';
?>


<div class="uk-width-1-2@m margen-v-50">
	<div id="fileuploader">
		Cargar
	</div>
</div>

<div class="uk-width-1-2@m margen-v-50">
	El archivo debe ser formato CSV<br>
	CSV = Valores separados por comas<br>
	No se deben poner comas adicionles dentro de los campos
</div>

<div class="uk-width-1-1">
	<p>Ejemplo:</p>
	<p>
		No. categoría, Código SAP, Título, Descripción, Título google, Descripción de google, Precio, Existencias, Activo, orden, Instagram, Imagen, Tipo De Autoparte, Marca Vehiculo, Modelo Vehiculo, Año Vehiculo<br>
		1, LED002, Lámpara led, Lámpara led para todo tipo de vehículo. Incluye instalación., Lámpara led de venta en Guadalajara, Lámpara led para todo tipo de vehículo. Incluye instalación. Disponible en la ciudad de Guadalajara, 1000.00, 50, 1, 1, https://www.instagram.com/p/BlyPdCtH0LM/?utm_source=ig_web_button_share_sheet, , Pick Up, Ford, Lobo, 2017
	</p>
	<a href="../img/contenido/importar/ejemplo.csv" download class="uk-button uk-button-white"><i class="fa fa-download"></i> Ejemplo</a>
</div>


<?php
if (isset($showTable)) {
	echo '
	<div class="uk-width-1-1">
		<div class="uk-margin uk-text-center">
			<a href="index.php?rand='.rand(1,1000).'&seccion=productos&subseccion=importar&importardatos&file='.$fileFinal.'" class="uk-button uk-button-primary">Continuar</a>
		</div>
		<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-text-center" id="tablaproductos">
			<thead>
				<tr>
					<th onclick="sortTable(0)">No. categoría</th>
					<th onclick="sortTable(1)">Código SAP</th>
					<th onclick="sortTable(2)">Título</th>
					<th onclick="sortTable(3)">Descripción</th>
					<th onclick="sortTable(4)">Título google</th>
					<th onclick="sortTable(5)">Descripción de google</th>
					<th onclick="sortTable(6)">Precio</th>
					<th onclick="sortTable(7)">Existencias</th>
					<th onclick="sortTable(8)">Activo</th>
					<th onclick="sortTable(9)">orden</th>
					<th onclick="sortTable(10)">Instagram</th>
					<th onclick="sortTable(11)">Imagen</th>
					<th onclick="sortTable(12)">Tipo De Autoparte</th>
					<th onclick="sortTable(13)">Marca Vehiculo</th>
					<th onclick="sortTable(14)">Modelo Vehiculo</th>
					<th onclick="sortTable(15)">Año Vehiculo</th>
				</tr>
			</thead>';
	foreach ($infoImportar as $key => $value) {
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $value[0]");
		$rowCONSULTA = $CONSULTA -> fetch_assoc();
		$catName=$rowCONSULTA['txt'];

		if (strlen($value[10])>0) {
			$texto=file_get_contents(trim($value[10]));
			$texto=strstr($texto,'og:image');
			$texto=strstr($texto,'htt');
			$value[11] =strstr($texto,'"',true);
		}

		echo "
			<tbody>
				<tr>
					<td>$catName</td>
					<td>$value[1]</td>
					<td>$value[2]</td>
					<td>$value[3]</td>
					<td>$value[4]</td>
					<td>$value[5]</td>
					<td>$value[6]</td>
					<td>$value[7]</td>
					<td>$value[8]</td>
					<td>$value[9]</td>
					<td>$value[10]</td>
					<td><img src='$value[11]' style='max-height:50px;'></td>
					<td>$value[12]</td>
					<td>$value[13]</td>
					<td>$value[14]</td>
					<td>$value[15]</td>
				</tr>
			</tbody>
			";
	}
	echo '
		</table>
	</div>';
}
?>



<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


<?php
$scripts='
	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "csv",
			maxFileSize: 9999999,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&csvfile=\'+data);
			}
		});

		// Eliminar todo los productos
		$("#eliminatodo").click(function() {
			UIkit.modal.confirm("Desea borrar todos los productos?").then(function () {
				var statusConfirm2 = confirm("Perdona la insistencia, pero es muy importante. Estás a punto de borrar todos los productos. Estás seguro?"); 
				if (statusConfirm2 == true) {
					$.post("modulos/'.$seccion.'/acciones.php",{
						borrartodoslosproductos: 1
					},function(msg){
						datos = JSON.parse(msg);
						UIkit.notification.closeAll();
						UIkit.notification(datos.msg);
					});
				}
			}, function () {
				console.log("Rejected.")
			});
		});

	});	
	';



?>