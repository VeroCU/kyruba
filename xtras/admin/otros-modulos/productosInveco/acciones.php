<?php
	$seccion='productos';
	$seccioncat=$seccion.'cat';
	$seccionpic=$seccion.'pic';
	$seccionmain=$seccion.'main';
	$rutaFinal='../img/contenido/'.$seccion.'/';

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nuevo Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevo'])){ 
		// Insertar en la base de datos
		$sql = "INSERT INTO $seccion (fecha)".
			"VALUES ('$hoy')";
		if($insertar = $CONEXION->query($sql)){
			$exito=1;
			$legendSuccess .= "<br>Producto nuevo";
			$editarNuevo=1;
			$id=$CONEXION->insert_id;
			$subseccion='detalle';
		}else{
			$fallo=1;  
			$legendFail .= "<br>No se pudo agregar a la base de datos";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Editar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['editar']) OR isset($editarNuevo)) {
		foreach ($_POST as $key => $value) {
			if ($key=='txt' OR $key=='txt1') {
				$dato = trim(str_replace("'", "&#039;", $value));
			}else{
				$dato = trim(htmlentities($value, ENT_QUOTES));
			}
			$actualizar = $CONEXION->query("UPDATE $seccion SET $key = '$dato' WHERE id = $id");
			$exito=1;
			unset($fallo);
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarPod'])){
		$rutaIMG="../img/contenido/".$seccionmain."/";

		$consulta= $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$rowConsulta = $consulta-> fetch_assoc();

		$pic=$rowConsulta['imagen'];
		$picRow=$rutaIMG.$pic;

		if (strlen($pic)>0 AND file_exists($picRow)) {
			unlink($picRow);
		}


		$rutaIMG="../img/contenido/".$seccion."/";

		$consulta= $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $id");
		$rowConsulta = $consulta-> fetch_assoc();

		$pic=$rowConsulta['id'].'-orig.jpg';
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
		}
		$pic=$rowConsulta['id'].'-sm.jpg';
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
		}

		if($borrar = $CONEXION->query("DELETE FROM $seccion WHERE id = $id")){
			$borrar = $CONEXION->query("DELETE FROM $seccionpic WHERE producto = $id");
			$exito=1;
			$legendSuccess .= "<br>Producto eliminado";
		}else{
			$legendFail .= "<br>No se pudo borrar de la base de datos";
			$fallo=1;  
		}
	}


//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto Redes    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarpicredes'])){
		$rutaFinal="../img/contenido/".$seccionmain."/";
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['imagen'])>0) {
			unlink($rutaFinal.$row_CONSULTA['imagen']);
			$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Foto eliminada';
		}else{
			$legendFail .= "<br>No se encontró la imagen en la base de datos";
			$fallo=1;
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar PDF    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarpdf'])){
		$rutaFinal="../img/contenido/".$seccion."pdf/";
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['pdf'])>0) {
			unlink($rutaFinal.$row_CONSULTA['pdf']);
			$actualizar = $CONEXION->query("UPDATE $seccion SET pdf = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Ficha eliminada';
		}else{
			$legendFail .= "<br>No se encontró en la base de datos";
			$fallo=1;
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto Redes    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarpicgallery'])){
		include '../../../includes/connection.php';
		$rutaIMG="../../../img/contenido/".$seccion."/";
		$id=$_POST['id'];
		$mensaje='';


		$pic=$id.'-orig.jpg';
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
			$exito=1;
		}else{
			$mensaje.='<br>No existe orig';
		}
		$pic=$id.'-sm.jpg';
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
			$exito=1;
		}else{
			$mensaje.='<br>No existe sm';
		}

		if (isset($exito)) {
			$borrar = $CONEXION->query("DELETE FROM $seccionpic WHERE id = $id");
			$mensajeClase='success';
			$mensajeIcon='check';
			$mensaje.='Borrado';
		}else{
			$mensajeClase='danger';
			$mensajeIcon='exclamation-triangle';
			$mensaje.='<br>No se pudo borrar';
		}
		echo '<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>';		
	}
//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nueva Categoria     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevacategoria'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['categoria'])) { $categoria=$_POST['categoria'];   }else{	$categoria=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$categoria=(htmlentities($categoria, ENT_QUOTES));

		// Actualizamos la base de datos
		if($categoria!=""){
			$sql = "INSERT INTO $seccioncat (txt,parent) VALUES ('$categoria',0)";
			if($insertar = $CONEXION->query($sql)){
				$cat = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Nueva categoria";
			}else{
				$fallo=1;
				$legendFail .= "<br>No se puede guardar";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nueva Subcategoria     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevasubcategoria'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['categoria'])) { $categoria=$_POST['categoria'];   }else{	$categoria=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$categoria=(htmlentities($categoria, ENT_QUOTES));

		// Actualizamos la base de datos
		if($categoria!=""){
			$sql = "INSERT INTO productossub (txt) VALUES ('$categoria')";
			if($insertar = $CONEXION->query($sql)){
				$cat = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Nueva categoria";
			}else{
				$fallo=1;
				$legendFail .= "<br>No se puede guardar";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nueva Subategoria     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevasubcategoria'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['categoria'])) { $categoria=$_POST['categoria'];   }else{	$categoria=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$categoria=htmlentities($categoria, ENT_QUOTES);

		// Actualizamos la base de datos
		if($categoria!=""){
			$sql = "INSERT INTO $seccioncat (txt,parent) VALUES ('$categoria',$cat)";
			if($insertar = $CONEXION->query($sql)){
				$exito=1;
				$legendSuccess .= "<br>Nueva subcategoria";
			}else{
				$fallo=1;  
				$legendFail .= "<br>No pudo agregarse a la base de datos ".$seccioncat.'-'.$cat.'-'.$categoria;
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['eliminacat'])){
		$rutaIMG="../img/contenido/".$seccioncat."/";

		$consulta= $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
		$rowConsulta = $consulta-> fetch_assoc();

		$pic=$rowConsulta['imagen'];
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
		}

		$pic=$rowConsulta['imagenhover'];
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
		}

		if($borrar = $CONEXION->query("DELETE FROM $seccioncat WHERE id = $cat")){
			$exito=1;
			$legendSuccess .= "<br>Categoria eliminada";
		}else{
			$fallo=1;
			$legendFail .= "<br>No se pudo borrar de la base de datos";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar varios tipos de dato     %%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['eliminargeneral'])){
		$tabla=$_GET['tabla'];
		if($borrar = $CONEXION->query("DELETE FROM $tabla WHERE id = $id")){
			$exito=1;
			$legendSuccess .= "<br>Eliminado";
		}else{
			$fallo=1;
			$legendFail .= "<br>No se pudo borrar de la base de datos";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir foto galería    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_FILES["uploadedfile"])){
		include '../../../includes/connection.php';
		$rutaFinal = '../../../img/contenido/'.$seccion.'/';

		$id=$_GET['id'];
		$sql = "INSERT INTO $seccionpic (producto) VALUES ($id)";
		if($insertar = $CONEXION->query($sql)){
			$picId=$CONEXION->insert_id;

			$ret = array();
			
			$error = $_FILES["uploadedfile"]["error"];
			//You need to handle  both cases
			//If Any browser does not support serializing of multiple files using FormData() 
			if(!is_array($_FILES["uploadedfile"]["name"])) //single file
			{
		 	 	$archivoInicial = $_FILES["uploadedfile"]["name"];
				$i = strrpos($archivoInicial,'.');
				$l = strlen($archivoInicial) - $i;
				$ext = strtolower(substr($archivoInicial,$i+1,$l));

		 	 	$archivoFinal  =$picId.'.'.$ext;
		 		move_uploaded_file($_FILES["uploadedfile"]["tmp_name"],$rutaFinal.$archivoFinal);
		    	$ret[]= $archivoFinal;
			}
			else  //Multiple files, file[]
			{
			  $fileCount = count($_FILES["uploadedfile"]["name"]);
			  for($i=0; $i < $fileCount; $i++)
			  {
			  	$archivoInicial = $_FILES["uploadedfile"]["name"][$i];
				$i = strrpos($archivoInicial,'.');
				$l = strlen($archivoInicial) - $i;
				$ext = strtolower(substr($archivoInicial,$i+1,$l));

			  	$archivoFinal  =$picId.'.'.$ext;
				move_uploaded_file($_FILES["uploadedfile"]["tmp_name"][$i],$rutaFinal.$archivoFinal);
			  	$ret[]= $archivoFinal;
			  }
			}
			//$actualizar = $CONEXION->query("UPDATE $seccionpic SET file = '$archivoFinal' WHERE id = $picId");
			echo json_encode($ret);



		    // %%%%%%%%%%%%%%%%%%%
		    //    MINIATURAS
		    // %%%%%%%%%%%%%%%%%%%
				$xs=1;
				$sm=1;
				$lg=1;

				// Leer el archivo para hacer la nueva imagen
				$original = imagecreatefromjpeg($rutaFinal.$archivoFinal);

				// Tomamos las dimensiones de la imagen original
				$ancho  = imagesx($original);
				$alto   = imagesy($original);


				if ($xs==1) {
					//  Imagen xs
					$miniaturaName=$rutaFinal.$picId."-xs.jpg";
					$anchoNuevo = 80;
					$altoNuevo  = $anchoNuevo*$alto/$ancho;

					// Creamos la imagen
					$archivoFinal = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
					// Copiamos el contenido de la original para pegarlo en el archivo nuevo
					imagecopyresampled($archivoFinal,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
					// Pegamos el contenido de la imagen
					if(imagejpeg($archivoFinal,$miniaturaName,90)){ // 90 es la calidad de compresión
						$exito=1;
					}
				}

				if ($sm==1) {
					//  Imagen sm
					$miniaturaName=$rutaFinal.$picId."-sm.jpg";
					$anchoNuevo = 400;
					$altoNuevo  = $anchoNuevo*$alto/$ancho;

					// Creamos la imagen
					$archivoFinal = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
					// Copiamos el contenido de la original para pegarlo en el archivo nuevo
					imagecopyresampled($archivoFinal,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
					// Pegamos el contenido de la imagen
					if(imagejpeg($archivoFinal,$miniaturaName,90)){ // 90 es la calidad de compresión
						$exito=1;
					}
				}

				if ($lg==1) {
					//  Imagen lg
					$miniaturaName=$rutaFinal.$picId."-lg.jpg";
					if ($ancho>$alto) {
						$anchoNuevo = 1000;
						$altoNuevo  = $anchoNuevo*$alto/$ancho;
					}else{
						$altoNuevo  = 1000;
						$anchoNuevo = $altoNuevo*$ancho/$alto;
					}

					// Creamos la imagen
					$archivoFinal = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
					// Copiamos el contenido de la original para pegarlo en el archivo nuevo
					imagecopyresampled($archivoFinal,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
					// Pegamos el contenido de la imagen
					if(imagejpeg($archivoFinal,$miniaturaName,90)){ // 90 es la calidad de compresión
						$exito=1;
					}
				}

		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar foto galería    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['borrarfoto'])){
		include '../../../includes/connection.php';
		$rutaFinal='../../../img/contenido/'.$seccion.'/';
		$id=$_POST['id'];
		// Borramos el archivo de imagen
		$filehandle = opendir($rutaFinal); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != "..") {
				// Id de la imagen
				if (strpos($file,'-')===false) {
					$imagenID = strstr($file,'.',TRUE);
				}else{
					$imagenID = strstr($file,'-',TRUE);
				}
				// Comprobamos que sean iguales
				if($imagenID==$id){
					$pic=$rutaFinal.$file;
					$exito=1;
					unlink($pic);
				}
			}
		}

		$borrar = $CONEXION->query("DELETE FROM $seccionpic WHERE id = $id");
		if(isset($exito)){
			echo "<div class='bg-success color-blanco'><i uk-icon='icon: trash;ratio:2;'></i> &nbsp; Borrado</div>";
		}else{
			echo "<div class='bg-danger color-blanco'><i uk-icon='icon: warning;ratio:2;'></i> &nbsp; No se pudo borrar</div>";
		}
	}


//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir Imágen Otros      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_GET['imagen'])){
		$position=$_GET['position'];

		$xs=0;
		$sm=1;
		$lg=0;
		$rutaFinal=($position=='main')?'../img/contenido/'.$seccion.'main/':'../img/contenido/'.$seccion.'/';


		//Obtenemos la extensión de la imagen
		$rutaInicial="../library/upload-file/php/uploads/";
		$imagenName=$_REQUEST['imagen'];
		$i = strrpos($imagenName,'.');
		$l = strlen($imagenName) - $i;
		$ext = strtolower(substr($imagenName,$i+1,$l));


		// Guardar en la base de datos
		if (!isset($fallo)) {
			if(file_exists($rutaInicial.$imagenName)){
				$pic=$id;
				if ($position=='gallery') {
					$sql = "INSERT INTO $seccionpic (producto) VALUES ($id)";
					$insertar = $CONEXION->query($sql);
					$pic=$CONEXION->insert_id;
				}elseif($position=='color'){
					$rutaFinal='../img/contenido/'.$seccion.'color/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$imgFinal=rand(111111111,999999999).'.'.$ext;
					}
					$sql = "INSERT INTO productoscolor (imagen) VALUES ('$imgFinal')";
					$insertar = $CONEXION->query($sql);
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
				}elseif($position=='categoria'){
					$rutaFinal='../img/contenido/'.$seccion.'cat/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$imgFinal=rand(111111111,999999999).'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['imagen']!='' AND file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
						unlink($rutaFinal.$row_CONSULTA['imagen']);
					}
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccioncat SET imagen = '$imgFinal' WHERE id = $cat");
				}elseif($position=='categoria2'){
					$rutaFinal='../img/contenido/'.$seccion.'cat/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$imgFinal=rand(111111111,999999999).'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['imagenhover']!='' AND file_exists($rutaFinal.$row_CONSULTA['imagenhover'])) {
						unlink($rutaFinal.$row_CONSULTA['imagenhover']);
					}
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccioncat SET imagenhover = '$imgFinal' WHERE id = $cat");
				}elseif($position=='main'){
					$rutaFinal='../img/contenido/'.$seccionmain.'/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$imgFinal=rand(111111111,999999999).'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['imagen']!='' AND file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
						unlink($rutaFinal.$row_CONSULTA['imagen']);
					}
					$legendFail.='<br>Fail - '.$position;
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '$imgFinal' WHERE id = $id");
				}elseif($position=='pdf'){
					$rutaFinal='../img/contenido/'.$seccion.'pdf/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$imgFinal=rand(111111111,999999999).'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['pdf']!='' AND file_exists($rutaFinal.$row_CONSULTA['pdf'])) {
						unlink($rutaFinal.$row_CONSULTA['pdf']);
					}
					$legendFail.='<br>Fail - '.$position;
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccion SET pdf = '$imgFinal' WHERE id = $id");
				}
			}else{
				$fallo=1;
				$legendFail='<br>No se permite refrescar la página.';
			}
		}

		// Borramos las imágenes que estén remanentes en el directorio files
		$filehandle = opendir($rutaInicial); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != ".." && $file != ".gitignore" && $file != ".htaccess" && $file != "thumbnail") {
				if(file_exists($rutaInicial.$file)){
					//echo $ruta.$file.'<br>';
					unlink($rutaInicial.$file);
				}
			}
		} 
		closedir($filehandle);
	}


	// Borramos archivos de datos importados
	if (isset($_GET['deleteprev'])) {
		$rutaImportar='../img/contenido/importar/';
		$filehandle = opendir($rutaImportar); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != ".." && $file != "ejemplo.csv") {
				if(file_exists($rutaImportar.$file)){
					//echo $ruta.$file.'<br>';
					unlink($rutaImportar.$file);
				}
			}
		} 
		closedir($filehandle);
	}

