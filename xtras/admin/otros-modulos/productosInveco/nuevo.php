<?php

	echo '
	<div class="uk-width-1-1 margin-v-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo" class="color-red">Nuevo</a></li>
		</ul>
	</div>';

?>

<form action="index.php" class="uk-width-1-1" method="post" name="editar" onsubmit="return checkForm(this);">
	<input type="hidden" name="nuevo" value="1">
	<input type="hidden" name="seccion" value="<?=$seccion?>">

	<div uk-grid class="uk-grid-small uk-child-width-1-3@l uk-child-width-1-2@m">
		<div>
			<label class="uk-text-uppercase" for="sku">sku</label>
			<input type="text" class="uk-input" name="sku" placeholder="sku">
		</div>
		<div>
			<label class="uk-text-capitalize" for="titulo">titulo</label>
			<input type="text" class="uk-input" name="titulo" required>
		</div>
		<div>
			<label class="uk-text-capitalize" for="precio">precio</label>
			<input type="number" class="uk-input" name="precio" placeholder="Ej. 1850" min="0">
		</div>
		<div>
			<label class="uk-text-capitalize" for="descuento">descuento</label>
			<input type="number" class="uk-input descuento" name="descuento" placeholder="Ej. 10" value="0" max="100" min="0">
		</div>

		<div class="uk-width-1-1">
			<div class="margin-top-20">
				<label for="txt">Descripción</label>
				<textarea class="editor" name="txt"></textarea>
			</div>
		</div>

		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="title">titulo google</label>
			<input type="text" class="uk-input" name="title" placeholder="Término como alguien nos buscaría">
		</div>
		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="metadescription">descripción google</label>
			<textarea class="uk-textarea" name="metadescription" placeholder="Descripción explícita para que google muestre a quienes nos vean en las búsquedas"></textarea>
		</div>
		<div class="uk-width-1-1 uk-text-center">
			<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
			<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
		</div>
	</div>
</form>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


<?php $scripts='
$(function(){
	$("#datepicker").datepicker();
	$( "#datepicker" ).datepicker( "option", "dateFormat", "yy-mm-dd" );
});
'; ?>