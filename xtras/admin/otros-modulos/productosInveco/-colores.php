<?php
$cat=1;
echo '

<div class="uk-width-1-1 margin-top-20 uk-text-left">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=colores" class="color-red"><i uk-icon="icon:cog"></i></a></li>
	</ul>
</div>';




echo '
<div class="uk-width-1-1 uk-text-center">
	<h3>Colores y texturas</h3>
</div>
<div class="uk-width-1-2@s margin-v-20">
	<div class="uk-card uk-card-default uk-card-body uk-border-rounded">
		<form action="index.php" class="uk-width-1-1 uk-text-center" method="post" name="editar" onsubmit="return checkForm(this);">
			<div uk-grid>
				<div class="uk-width-expand">
					<input type="hidden" name="nuevocolor" value="1">
					<input type="hidden" name="seccion" value="'.$seccion.'">
					<input type="hidden" name="subseccion" value="'.$subseccion.'">
					<input type="color" name="txt" class="uk-input" value="#ffffff" required><br><br>
				</div>
				<div class="uk-width-auto">
					<input type="submit" name="send" value="Agregar" class="uk-button uk-button-primary">
				</div>
			</div>
		</form>
	</div>
</div>
<div class="uk-width-1-2@s margin-v-20 uk-text-center">
	<div class="uk-card uk-card-default uk-card-body uk-border-rounded">
		<a href="#colorpic" uk-toggle class="uk-button uk-button-primary">Nueva textura</a>
	</div>
</div>
<div class="uk-width-1-1">
	<div>
		<div uk-grid class="uk-flex-center uk-text-center">';

	// Obtener colores
	$CONSULTA = $CONEXION -> query("SELECT * FROM productoscolor ORDER BY txt");
	while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {
		$thisID   = $rowCONSULTA['id'];
		$imagen   = '../img/contenido/productoscolor/'.$rowCONSULTA['imagen'];
		$colorTxt = (strlen($rowCONSULTA['imagen'])>0 AND file_exists($imagen))?'<div class="uk-border-circle uk-container" style="background:url('.$imagen.');background-size:cover;width:70px;height:70px;border:solid 1px #999;">&nbsp;</div>':'<input type="color" class="editarcolor uk-input uk-form-width-xsmall" data-tabla="productoscolor" data-campo="txt" data-id="'.$rowCONSULTA['id'].'" placeholder="Color" value="'.$rowCONSULTA['txt'].'">';
		echo '
			<div style="max-width:200px;">
				<div class="uk-card-body">
					<div>
						<input class="editarajax uk-input" data-tabla="productoscolor" data-campo="name" data-id="'.$thisID.'" value="'.$rowCONSULTA['name'].'">
					</div>
					<div class="uk-margin">
						'.$colorTxt.'
					</div>
					<div class="uk-text-center">
						<button data-id="'.$thisID.'" data-tabla="productoscolor" data-campo="color" class="borrarexistencias uk-icon-button uk-button-danger" uk-icon="icon:trash"></a>
					</div>
				</div>
			</div>';
	}


	echo '
		</div>
	</div>
</div>


<div class="padding-v-50">
</div>
<div class="padding-v-50">
</div>


<div id="ficha" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<button class="uk-modal-close-default" type="button" uk-close></button>
		<input type="hidden" id="fichaid">
		<p>PNG 50 x 50 px</p>
		<div id="fileupload">
			Cargar
		</div>
	</div>
</div>
<div id="banner" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<button class="uk-modal-close-default" type="button" uk-close></button>
		<input type="hidden" id="bannerid">
		<p>JPG 1500 x 400 px</p>
		<div id="bannerupload">
			Cargar
		</div>
	</div>
</div>
<div id="colorpic" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<button class="uk-modal-close-default" type="button" uk-close></button>
		<p>JPG 50 x 50 px</p>
		<div id="colorupload">
			Cargar
		</div>
	</div>
</div>




<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>



';


$scripts='
	// Eliminar
	$(".borrarexistencias").click(function(){
		var id = $(this).attr("data-id");
		var tabla = $(this).attr("data-tabla");
		var campo = $(this).attr("data-campo");
		UIkit.modal.confirm("Desea eliminar esto?").then(function() {
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&eliminargeneral=1&tabla="+tabla+"&campo="+campo+"&id="+id);
		}, function () {
		    console.log("Rejected.")
		});
	});
	
	$("#colorupload").uploadFile({
		url: "../library/upload-file/php/upload.php",
		fileName: "myfile",
		maxFileCount: 1,
		showDelete: \'false\',
		allowedTypes: "jpg,jpeg",
		maxFileSize: 20000000,
		showFileCounter: false,
		showPreview: false,
		returnType: \'json\',
		onSuccess:function(data){
			console.log(data);
			window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&position=color&imagen=\'+data);
		}
	});

	// Editor color
	$(".editarcolor").change(function() {
		var id = $(this).attr("data-id");
		var tabla = $(this).attr("data-tabla");
		var campo = $(this).attr("data-campo");
		var valor = $(this).val();

		$.ajax({
			method: "POST",
			url: "modulos/varios/acciones.php",
			data: { 
				editarajax: 1,
				id: id,
				tabla: tabla,
				campo: campo,
				valor: valor
			}
		})
		.done(function( msg ) {
			UIkit.notification.closeAll();
			UIkit.notification(msg);
		});
	});

	';
