<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<section class="bg_espacio">
	<?=$header?>
	<div class="uk-container uk-flex uk-flex-center uk-flex-middle">
		<div class="uk-width-1-3 uk-background-contain uk-height-medium height_120" style="background-image: url(./img/design/logo-blanco.png);"></div>   
		<!--div class="uk-width-1-1 uk-card uk-card-default uk-card-body">
			<div uk-grid class="uk-child-width-1-3@s">
				<div>
					<label>Nombre
					<input type="text" class="uk-input input-personal" id="footernombre"></label>
				</div>
				<div>
					<label>Correo
					<input type="email" class="uk-input input-personal" id="footeremail"></label>
				</div>
				<div>
					<label>Teléfono
					<input type="text" class="uk-input input-personal" id="footertelefono"></label>
				</div>
			</div>
			<div class="margin-top-20">
				<div>
					<label>Mensaje
					<textarea type="text" class="uk-textarea input-personal" id="footercomentarios"></textarea></label>
				</div>
			</div>
			<div class="margin-top-20 uk-text-center">
				<button class="uk-button uk-button-personal footer-enviar" id="footersend">Enviar</button>
			</div>
		</div-->
	</div>


	<div class="uk-width-1-1 slider_principal" uk-slider>
	    <div class="uk-position-relative">
	        <div class="uk-slider-container uk-light">
	            <ul class="uk-slider-items uk-child-width-1-1">
	                <li>
	                    <div class="uk-width-1-1 uk-background-contain uk-height-medium " style="background-image: url(./img/design/slider_1.png);"></div>
	                </li>
	                <li>
	                    <div class="uk-width-1-1 uk-background-contain uk-height-medium " style="background-image: url(./img/design/logo-blanco.png);"></div>
	                </li>
	            </ul>
	        </div>
	        <div class="margin-h-15">
				<a href="registro" class="uk-button uk-button-blanco">
					Saber más
				</a>
			</div>
	        <div class="uk-hidden@s uk-light">
	            <a class="uk-position-center-left uk-position-small uk-previous" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
	            <a class="uk-position-center-right uk-position-small uk-next" href="#" uk-slidenav-next uk-slider-item="next"></a>
	        </div>

	        <div class="uk-visible@s contain_flecha" style="">
	            <a class="uk-position-center-left-out uk-position-small uk-previous" href="#" uk-slidenav-previous uk-slider-item="previous" style=""></a>
	            <a class="uk-position-center-right-out uk-position-small uk-next" href="#" uk-slidenav-next uk-slider-item="next" style=""></a>
	        </div>

	    </div>
	</div>
	<section class="uk-grid-collapse container_table_circle">
		<div class="uk-container" style="">
			<div class="uk-grid" uk-grid style="">
				<div class="uk-width-1-2 tabla" style="">
					<div class="uk-background-contain uk-height-medium uk-panel bg_tabla"  
						style="background-image: url(./img/design/bg_border.png);">
						<div class="uk-grid" uk-grid style="">
							<div class="uk-width-1-1 uk-grid-collapse uk-text-truncate titles" >
								Proximo Eventos
							</div>
							<div class="uk-width-1-1 uk-grid-collapse uk-text-truncate titles">
								FEBRERO 2020
							</div>
							<div class="uk-width-1-1 uk-grid-collapse table">
								<div class="uk-grid" uk-grid style="">
									<div class="uk-width-1-1 container-row" style=";">
										<div class="uk-grid" uk-grid style="">
											<div class="uk-width-1-4 t_left" style="">
												Lunes 3<br>
												Martes 4<br>
												Viernes 18<br>
												Lunes 23<br>
												Sabado 27
											</div>
											<div class="uk-width-expand t_right">
												Taller de numerología<br>
												Taller de neuromarketing<br>
												Concierto Miedo<br>
												Conferencia Pronoia Consciente<br>
												Los arqueotipos del lider curso taller
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-1-2 uk-flex uk-flex uk-flex-center uk-flex-middle ">
					<div class="circle">
						<div class="uk-border-circle container-circle" style="">
							<div class="uk-grid" uk-grid style="">
								<div class="uk-width-1-6 uk-align-right c_right">
									<span uk-icon="icon: quote-right; ratio: 3"></span>
								</div>
								<div class="uk-width-expand c_left">
									<div class="uk-flex uk-flex-middle c_scroll" 
									style="">
										En el mundo común de los hechos, los malos no son castigados y los buenos recompensados.
										El éxito se lo llevan los fuertes y el fracaso los débiles.
									</div>
									<span style="color:#fff">Oscar Wilde</span><br>
									<span uk-icon="icon: more; ratio:1.8" style="color:#fff;margin-left:50px"></span>
									<span uk-icon="icon: quote-right; ratio:1.8" style="color:#fff"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<style type="text/css">
	.uk-open>.uk-accordion-title::before{
		background-image:url(./img/design/close.png);
	}
	.uk-accordion-title::before{
		background-image:url(./img/design/flecha.png);
	}		
</style>
	<section class="bg_center">
		<div class="uk-width-1-1 contain_acordion">
			<div class="uk-container uk-text-center">
				<h1 style="color:#fff;font-size:70px;font-weight:bold;">EVENTOS DESTACADOS</h1>
				<div class="uk-flex uk-flex-center">
					<hr style="border-top:solid #fff;width:60px">
				</div>
				<div class="uk-flex uk-flex-center">
					<div class="uk-width-1-3" style="color:#fff; font-size:18px">
						En el mundo común de los hechos, los malos no son castigados y los buenos recompensados. El éxito se lo llevan los fuertes y el fracaso los débiles.
					</div>
				</div>
			</div>
			<div class="uk-container uk-text-center">
				<ul class="acordion" uk-accordion="multiple: true" style="">
				    <li class="uk-open">
				        <a class="uk-accordion-title uk-text-uppercase oswald " href="#"><span class="a_title">CONFERENCIAS</span></a>
				        <div class="uk-accordion-content">
						    <div class="uk-grid" uk-grid style="padding-left:0;"> 
								<div class="uk-width-expand" style="border:solid blue">
									<div uk-slider>
								    <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
								        <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m">
								        	<?php  
								        		for($i=1; $i<6; $i++):
											?>
								            <li style="padding:10px;">
									            <a class="hover_elements" href="">
									                <div class="container_vista">
									                	<img src="./img/design/conferencia.jpg" style="height: 260px;">
									                	<div class="container_v">
									                		Los milagros si existen
									                		<div style="">Pilar Martínez</div>
									                	</div>
									                	<div class="contain_h uk-flex uk-flex-middle">
									                		<div style="font-size:20px;color:#4084c5">Ver detalle</div>
									                		<div class="uk-flex uk-flex-center">
																<hr style="border-top:solid #4084c5;margin-top:20px;width:80px">
															</div>
									                		<div style="font-size:20px;color:#4084c5">Registrarse al evento</div>
									                	</div>
									                </div>
									            </a>
								            </li>
								            <?php
												endfor
											?>
								        </ul>

								        <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
								        <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>
								    </div>
								    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
									</div>
								</div>
								<div class="uk-width-auto" style="border:solid red;padding-left: 0;">hola</div>
							</div>  
				        </div>
				    </li>
				    <li>
				        <a class="uk-accordion-title uk-text-uppercase oswald a_title" href="#">PROFRAMA</a>
				        <div class="uk-accordion-content">
				            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor reprehenderit.</p>
				        </div>
				    </li>
				    <li>
				        <a class="uk-accordion-title uk-text-uppercase oswald a_title" href="#">CURSOS</a>
				        <div class="uk-accordion-content">
				            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat proident.</p>
				        </div>
				    </li>
				    <li>
				        <a class="uk-accordion-title uk-text-uppercase oswald a_title" href="#">PELÍCULAS</a>
				        <div class="uk-accordion-content">
				            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat proident.</p>
				        </div>
				    </li>
				</ul>
			</div>

			<div class="uk-container uk-text-center" style="">
				<div class="uk-grid" uk-grid>
						<div  class="uk-width-1-2">hola</div>
						<div class="uk-width-1-2 uk-flex uk-flex uk-flex-center uk-flex-middle" 
							style="padding:40px">
						<div>
							<div class="uk-border-circle" style="background:#635e9f;height:450px;width:450px;margin-bottom:-180px;">
								<div style="padding:40px 58px;">
									<h3 class="uk-text-uppercase oswald" style="color:#fff;font-size:30px;font-weight:600;margin-top:10px">INSCRÍBETE</h3>
									<div class="uk-flex uk-flex-center" style="height:1px">
										<hr style="height:1px;border-top:solid #fff;margin-bottom:50px;width:60px">
									</div>
									<div style="color:#fff;font-size:20px;height:60px;overflow:hidden;">
										¿Estas interesado en alguno de nuestros eventos? 
									</div>
									<div style="color:#fff;font-size:14px;height:80px;overflow:scroll;">
										Selecciona, la conferencia, Curso o Taller en el que estes interesado, registra tus datos y sigue los pasos y genera tu orden de pago, o bien paga en linea
									</div>
									<div class="uk-flex uk-flex-center" style="height:1px">
										<hr style="height:1px;border-top:solid #fff;margin-bottom:50px;width:60px">
									</div>
									<h3 class="uk-text-uppercase oswald" style="color:#fff;font-size:30px;font-weight:600;margin-top:20px">¿UNA PROBADITA?</h3>
									<div style="color:#fff;font-size:20px;height:60px;overflow:hidden;">
										Revisa nuestra material en linea, ¡solo tienes que registrarte! 
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>

<section class="bg_central" style="height:500px">
</section>

<?=$footer?>

<?=$scriptGNRL?>

</body>
</html>